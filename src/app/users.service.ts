import { Injectable } from '@angular/core';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {getLocaleDateFormat} from "@angular/common";

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http:HttpClient) { }

  getData() {
    let url = "https://jsonplaceholder.typicode.com/todos/";
    return this.http.get(url);
  }
}
